﻿using database_sql_assignment.Models;
using database_sql_assignment.Repositories;
using Sql_Database.Models;
using Sql_Database.Repositories;
using System;
using System.Collections.Generic;

namespace Sql_Database
{
    class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();
            TestGetCustomerByLimitAndOffset(repository, 10, 5);
        }

        /// <summary>
        /// This method iterate through the customers in a list from CustomerCountry model and show their country and the number of their customers
        /// </summary>
        /// <param name="customers"></param>
        static void PrintCustomerCountry(IEnumerable<CustomerCountry> customers)
        {
            foreach (CustomerCountry customer in customers)
            {
                Console.WriteLine($"--- {customer.Country}{customer.NumberOfCustomer} ---");
            }
        }

        /// <summary>
        /// This method iterate through the customers in a list from CustomerSpender model and show each customers' CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email, Totalof their invoices
        /// </summary>
        /// <param name="customers"></param>
        static void PrintCustomerSpender(IEnumerable<CustomerSpender> customers)
        {
            foreach (CustomerSpender customer in customers)
            {
                Console.WriteLine($"---{customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Country} {customer.PostalCode} {customer.Phone} {customer.Email} {customer.Total} ---");
            }
        }

        /// <summary>
        /// This method iterate through the customers in a list from CustomerGenre model and take the specific custom that has been required by id
        /// This method show that specific customers' the number of tracks from the populatest genre related to that customer and the populatest genre name for that customer
        /// </summary>
        /// <param name="customers"></param>
        static void PrintCustomerGenre(List<CustomerGenre> customers)
        {
            foreach (CustomerGenre customer in customers)
            {
                Console.WriteLine($"---{customer.Counts} {customer.GenName}---");
            }
        }

        /// <summary>
        /// This method iterate through the customers in a list from Customer model that shows all the customers's CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email
        /// </summary>
        /// <param name="customers"></param>
        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        /// <summary>
        /// This method take a custumer by a specific id and shows that specific customers' CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email
        /// </summary>
        /// <param name="customer"></param>
        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"---{customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Country} {customer.PostalCode} {customer.Phone} {customer.Email} ---");
        }

        /// <summary>
        /// This Method tests the fuctionality of the GetAllCustomers method and then it prints out all the customers
        /// </summary>
        /// <param name="repository"></param>
        static void TestSelectAll(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }

        /// <summary>
        /// This method tests the fuctionality of the GetCustomer method by getting a specific id and then it prints out that specific customer
        /// </summary>
        /// <param name="repository"></param>
        static void TestSelect(ICustomerRepository repository, int id)
        {
            PrintCustomer(repository.GetCustomer(id));
        }

        /// <summary>
        /// This method tests the fuctionality of the AddNewCustomer method by creating a new customer and add it
        /// It prints out "Added a new customer!" if it successfully insert a customer and prints out "could not add a new customer!" if it fails
        /// </summary>
        /// <param name="repository"></param>
        static void TestInsert(ICustomerRepository repository)
        {
            Customer test = new Customer()
            {
                FirstName = "Diego",
                LastName = "Peterson",
                Country = "Sweden",
                PostalCode = "1234",
                Phone = "12334567",
                Email = "fadi@email.com"
            };
            if (repository.AddNewCustomer(test))
            {
                Console.WriteLine("Added a new customer!");
            }
            else
            {
                Console.WriteLine("could not add a new customer!");
            }
        }

        /// <summary>
        /// This method tests the fuctionality of the UpdateCustomer method by changing in an already existing customer
        /// It prints out "Updated customer!" and that updated customer if it successfully updates that customer and prints out "could not update the customer!" if it fails
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="id"></param>
        static void TestUpdate(ICustomerRepository repository, int id) 
        { 
            Customer test = new Customer() 
            { 
                CustomerId = id, FirstName = "Sean", 
                LastName = "Peterson", 
                Country = "brzil", 
                PostalCode = "1234", 
                Phone = "12334567", 
                Email = "sean@email.com" 
            }; 
            if (repository.UpdateCustomer(test, id)) 
            { 
                Console.WriteLine("Updated customer!"); 
                PrintCustomer(repository.GetCustomer(60)); 
            } else { 
                Console.WriteLine("could not update the customer!"); 
            } 
        }

        /// <summary>
        ///  This method tests the fuctionality of the GetCustomerByName method by getting a name
        /// It prints out all the customers that have the given name and their CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="firstName"></param>
        static void TestGetByName(ICustomerRepository repository, string firstName)
        {
            PrintCustomers(repository.GetCustomerByName(firstName));
        }

        /// <summary>
        ///  This method tests the fuctionality of the GetCustomerByLimitAndOffset method by getting an offset and limit which limits the rows by offset and starts based on limit
        /// It prints out a list of customers based on given limit and offset with the customers' CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        static void TestGetCustomerByLimitAndOffset(ICustomerRepository repository, int limit, int offset)
        {
            PrintCustomers(repository.GetCustomerByLimitAndOffset(limit, offset));
        }

        /// <summary>
        /// This method tests the fuctionality of the GetNumberOfCustomers method 
        /// It prints out a list of all countries with the number of customers that each country has
        /// </summary>
        /// <param name="repository"></param>
        static void TestGetNumberOfCustomers(ICustomerCountryRepository repository)
        {
            PrintCustomerCountry(repository.GetNumberOfCustomers());
        }

        /// <summary>
        /// This method tests the fuctionality of the GetTopCustomerSpenders method 
        /// It prints out a list of all customers and their total invoices ín a descending order
        /// </summary>
        /// <param name="repository"></param>
        static void TestGetTopCustomerSpenders(ICustomerSpenderRepository repository)
        {
            PrintCustomerSpender(repository.GetTopCustomerSpenders());
        }

        /// <summary>
        /// This method tests the fuctionality of the GetTopGenre method by using a given id
        /// It prints out a list of the populatest genre for that specific customer with the total number of tracks of that genre
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="id"></param>
        static void TestGetTopGenre(ICustomerGenreRepository repository, int id)
        {
            PrintCustomerGenre(repository.GetTopGenre(id));

        }
    }
}
