INSERT INTO ThePower( powerName, powerDescription)
VALUES('Teleportation', 'Teleport across incredibly vast distance')

INSERT INTO ThePower( powerName, powerDescription)
VALUES('Flight', 'Ability to fly')

INSERT INTO ThePower( powerName, powerDescription)
VALUES('Wireless Communication', 'Connect to external communications')

INSERT INTO ThePower( powerName, powerDescription)
VALUES('Information Absorption', 'Quickly absorb and store information')


INSERT INTO Powers_Link_Superheros( powerId, superheroId)
VALUES(1,1)

INSERT INTO Powers_Link_Superheros( powerId, superheroId)
VALUES(2,1)

INSERT INTO Powers_Link_Superheros( powerId, superheroId)
VALUES(2,2)

INSERT INTO Powers_Link_Superheros( powerId, superheroId)
VALUES(3,2)

INSERT INTO Powers_Link_Superheros( powerId, superheroId)
VALUES(4,3)

