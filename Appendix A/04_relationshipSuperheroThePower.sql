CREATE TABLE Powers_Link_Superheros(
   powerId int NOT NULL,
   superheroId int NOT NULL);

ALTER TABLE Powers_Link_Superheros
ADD FOREIGN KEY (powerId) REFERENCES ThePower(id)

ALTER TABLE Powers_Link_Superheros
ADD FOREIGN KEY (superheroId) REFERENCES Superhero(id)

ALTER TABLE Powers_Link_Superheros
ADD PRIMARY KEY( powerId, superheroId);