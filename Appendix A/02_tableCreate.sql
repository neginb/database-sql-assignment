CREATE TABLE Superhero (
id int NOT NULL PRIMARY KEY IDENTITY(1,1),
superheroName nvarchar(50),
alias nvarchar(50),
origin nvarchar(50)
);

CREATE TABLE Assistant (
id int NOT NULL PRIMARY KEY IDENTITY(1,1),
assistantName nvarchar(50)
);

CREATE TABLE ThePower (
id int NOT NULL PRIMARY KEY IDENTITY(1,1),
powerName nvarchar(50),
powerDescription nvarchar(50)
);
