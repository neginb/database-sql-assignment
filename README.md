# SQL Assignment

This is a SQL assignment, where we generate data from a given database connected to SQL server. The data is collected through SQL Querys to fill the requirements from the assignment.

This project was generated with [.NET] version 5.0.

You can find the SQL Query scripts files in this project in folder "Appendix A", by runing this queries you can create a database called "SuperheroesDb". You will be able to run CRUD operations, like create, read, update and delete.  

## Install

```
git clone "https://gitlab.com/neginb/database-sql-assignment.git"
cd database-sql-assignment
run code

```

## Usage

Implement code to ConnectionHelper class
```
 public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.DataSource = <server>;
            connectionStringBuilder.InitialCatalog = <database>;
            connectionStringBuilder.IntegratedSecurity = true;
            connectionStringBuilder.Encrypt = false;
            return connectionStringBuilder.ConnectionString;
        }

```
The server name can be found in your Microsoft SQL Server Managemnet. Open the app and and connect to a database engine, then go to properieis where you will find yor server name. 

## Contributors
@FadiAkkaoui and @neginb

## Contributing

No contributions allowed.
