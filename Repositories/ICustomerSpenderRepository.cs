﻿using database_sql_assignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace database_sql_assignment.Repositories
{
    public interface ICustomerSpenderRepository
    {
        public List<CustomerSpender> GetTopCustomerSpenders();
    }
}
