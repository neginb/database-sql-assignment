﻿using database_sql_assignment.Models;
using Microsoft.Data.SqlClient;
using Sql_Database.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace database_sql_assignment.Repositories
{
    public class CustomerSpenderRepository: ICustomerSpenderRepository
    {
        /// <summary>
        /// Method that gets a list of top customer spenders from the database in a descending order by total
        /// </summary>
        /// <returns>List of top customer spender, if it is successful</returns>
        /// <returns>sqlexeption, if it fails</returns>
        public List<CustomerSpender> GetTopCustomerSpenders()
        {
            List<CustomerSpender> res = new List<CustomerSpender>();

            string sql = "SELECT Invoice.CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email, SUM(Total) AS Total" +
                " FROM Invoice" +
                " INNER JOIN Customer " +
                "ON Invoice.CustomerId = Customer.CustomerId " +
                "GROUP BY Invoice.CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "ORDER BY Total DESC";

            try
            {
                //Connect 
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                // Handle result
                                CustomerSpender customer = new CustomerSpender();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader["Country"] != DBNull.Value ? reader.GetString(3).ToString() : string.Empty;
                                customer.PostalCode = reader["PostalCode"] != DBNull.Value ? reader.GetString(4).ToString() : string.Empty;
                                customer.Phone = reader["Phone"] != DBNull.Value ? reader.GetString(5).ToString() : string.Empty;
                                customer.Email = reader.GetString(6);
                                customer.Total = reader.GetDecimal(7);
                                res.Add(customer);
                            }
                        }
                    }
                }

            }

            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return res;
        }


    }
}
