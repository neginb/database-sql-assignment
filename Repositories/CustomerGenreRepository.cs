﻿using database_sql_assignment.Models;
using Microsoft.Data.SqlClient;
using Sql_Database.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace database_sql_assignment.Repositories
{
    public class CustomerGenreRepository : ICustomerGenreRepository
    {
        /// <summary>
        /// Method that gets a list of top genre name and total number of tracks based on a specific customer from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of top genres and its tracks, if it is successful</returns>
        /// <returns>sqlexeption, if it fails</returns>

        public List<CustomerGenre> GetTopGenre(int id)
        {
            List<CustomerGenre> res = new List<CustomerGenre>();
            string sql = "SELECT TOP 1 WITH TIES COUNT (Genre.Name) as Counts, Genre.Name as GenName FROM Invoice INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId INNER JOIN Track " +
                         "ON InvoiceLine.TrackId = Track.TrackId INNER JOIN Genre ON Track.GenreId = Genre.GenreId " +
                         "WHERE Invoice.CustomerId = @CustId GROUP BY Genre.Name, Invoice.CustomerId ORDER BY Counts DESC";
            try
            {
                //Connect 
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustId", id);
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                // Handle result
                                CustomerGenre customer = new CustomerGenre();
                                customer.Counts = reader.GetInt32(0);
                                customer.GenName = reader.GetString(1);
                                res.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return res;
        }
    }
}
